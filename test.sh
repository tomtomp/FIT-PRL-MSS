#!/bin/bash

if [ $# -lt 2 ];then 
    echo "Usage: ./test.sh NUM_VALUES NUM_PROCESSORS";
    exit;
fi;

num_values=$1;
num_processors=$2;

mpic++ --prefix /usr/local/share/OpenMPI -o mss mss.cpp

# Create random testing vector.
dd if=/dev/urandom bs=1 count=$num_values of=./numbers &>/dev/null

mpirun --prefix /usr/local/share/OpenMPI -np $num_processors mss ./numbers

# Cleanup
rm -f mss numbers

