#!/bin/bash

if [ $# -lt 5 ]
then 
    echo "Usage: ./test.sh MIN_VALUES MAX_VALUES STEP_VALUES MIN_PROCESSORS MAX_PROCESSORS";
    exit;
fi;

min_values=$(($1 * 1000000))
max_values=$(($2 * 1000000))
step_values=$(($3 * 1000000))

min_processors=$4
max_processors=$5
step_processors=1

echo "localhost slots=${max_processors}" > ./hostfile

mpic++ --prefix /usr/local/share/OpenMPI -DDEBUG -o mss mss.cpp -O3

for num_vals in `seq ${min_values} ${step_values} ${max_values}`
do
    # Create random testing vector.
    dd if=/dev/urandom bs=${step_values} count=1 of=./numbers oflag=append conv=notrunc 2>&1 &>/dev/null
    for num_procs in `seq ${min_processors} ${step_processors} ${max_processors}`
    do
        result=`mpirun --prefix /usr/local/share/OpenMPI --hostfile ./hostfile -np $num_procs mss ./numbers`
	echo "${num_vals};${result}"
	echo "${num_vals};${result}" >> ./result.csv
    done
done

# Cleanup
rm -f mss numbers hostfile

