pdf("results_one.pdf")

myData <- read.csv("results_24.csv", sep=';', header=FALSE)
plot(myData, col="red", main="Runtime benchmark", ylab="Time [s]", xlab="Elements", xaxt="n", yaxt="n")
abline(lm(V2~V1, myData), col="red")

myData <- read.csv("results_16.csv", sep=';', header=FALSE)
points(myData, col="darkred")
abline(lm(V2~V1, myData), col="darkred")

myData <- read.csv("results_15.csv", sep=';', header=FALSE)
points(myData, col="green")
abline(lm(V2~V1, myData), col="green")

myData <- read.csv("results_8.csv", sep=';', header=FALSE)
points(myData, col="darkgreen")
abline(lm(V2~V1, myData), col="darkgreen")

myData <- read.csv("results_4.csv", sep=';', header=FALSE)
points(myData, col="blue")
abline(lm(V2~V1, myData), col="blue")

myData <- read.csv("results_2.csv", sep=';', header=FALSE)
points(myData, col="darkblue")
abline(lm(V2~V1, myData), col="darkblue")

myData <- read.csv("results_sequential.csv", sep=';', header=FALSE)
points(myData, col="pink")
abline(lm(V2~V1, myData), col="pink")

axis(1, seq(0, max(myData[1]), 50000000))
axis(2, seq(0, max(myData[2]), 0.5))
legend("bottomright", legend=c("24 Processors", "16 Processors", "15 Processors", "8 Processors", "4 Processors", "2 Processors", "Sequential"), col=c("red", "darkred", "green", "darkgreen", "blue", "darkblue", "pink"), pch=c(1, 1, 1, 1, 1, 1, 1), lty=c(1, 1, 1, 1, 1, 1, 1), inset=0.1)
dev.flush()

