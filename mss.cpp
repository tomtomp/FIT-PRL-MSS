/**
 * @file mss.cpp
 * @Author Tomas Polasek
 * @brief Implementation of Merge-splitting sort algorithm, using OpenMPI.
 */

#include <mpi.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <limits>
#include <algorithm>
#include <cmath>

/// Type of the sorted values.
using SortedType = uint8_t;
/// MPI type of the sorted values.
static constexpr MPI_Datatype SORTED_MPI_TYPE{MPI_BYTE};

/// Tag used for all messages.
static constexpr int MY_TAG{0};

/// Maximum value of any sorted element.
static constexpr SortedType MAX_SORT_VALUE{std::numeric_limits<SortedType>::max()};

static constexpr int PARAM_NUM_REQUIRED{2};
static constexpr std::size_t PARAM_FILENAME{1};
// When received, this size means that error occurred.
static constexpr int ERROR_SIZE{-1};

/// Load number from file
std::vector<SortedType> loadFile(const char *filename)
{
    std::vector<SortedType> result;

    std::ifstream file(filename);
    if (!file.is_open())
    {
        throw std::runtime_error(std::string("Error, unable to open input file \"") + filename + "\"");
    }

    // Reserve the string size upfront.
    file.seekg(0, std::ios::end);
    result.reserve(file.tellg());
    file.seekg(0, std::ios::beg);

    result.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    return result;
}

/// Print given number vector on one line.
void printInputNumbers(const std::vector<SortedType> &inputNumbers)
{
    for (auto &num : inputNumbers)
    {
        std::cout << static_cast<uint16_t>(num) << " ";
    }
    std::cout << std::endl;
}

/// Print given number vector one number per line.
void printOutputNumbers(const std::vector<SortedType> &outputNumbers)
{
    for (auto &num : outputNumbers)
    {
        std::cout << static_cast<uint16_t>(num) << std::endl;
    }
}

/// Pas the input number vector to be multiple of number of processors.
std::size_t padInputNumbers(std::vector<SortedType> &inputNumbers, std::size_t numProcs)
{
    std::size_t origSize{inputNumbers.size()};
    std::size_t toAdd = (numProcs - (origSize % numProcs)) % numProcs;

    for (std::size_t iii = 0; iii < toAdd; ++iii)
    {
        inputNumbers.push_back(MAX_SORT_VALUE);
    }

    return toAdd;
}

/// Split the number vector between processors.
void splitInputNumbers(const std::vector<SortedType> &inputNumbers, std::size_t elementsPerProcessor, std::size_t numProcs)
{
    //MPI_Request req;
    if (elementsPerProcessor * numProcs != inputNumbers.size())
    {
        std::cerr << "Distributing only " << elementsPerProcessor * numProcs << " out of " << inputNumbers.size() << std::endl;
    }

    const SortedType *data{inputNumbers.data()};
    for (std::size_t procId = 1; procId < numProcs; ++procId)
    {
        MPI_Send(data + procId * elementsPerProcessor, elementsPerProcessor, SORTED_MPI_TYPE,
                 procId, MY_TAG, MPI_COMM_WORLD);
    }
}

/// Run merge-splitting sort algorithm. Started on every processor.
void mergeSplittingSort(std::vector<SortedType> &myNumbers, std::size_t myId, std::size_t numProcs)
{
    /*
     * The plan:
     *   1) Each processor sorts its own buffer with sequential algorithm (std::sort).
     *   2) Run steps sequentially, for k = 1 to ceil(numProcs / 2.0):
     *     I) All processors with odd IDs merge with neighbour and merge, then redistribute.
     *     II) All processors with even IDs merge with neighbour and merge, then redistribute.
     */

    std::sort(myNumbers.begin(), myNumbers.end());

    /// Status used for getting information from MPI functions.
    MPI_Status status;

    std::size_t elementsPerProcessor{myNumbers.size()};
    // Buffer used for receiving data from neighbor.
    std::vector<SortedType> receiveBuffer(elementsPerProcessor);
    // Buffer used for merging.
    std::vector<SortedType> mergeBuffer(elementsPerProcessor * 2);

    // Calculate how many steps does the algorithm have to make.
    const std::size_t numSteps = std::ceil(numProcs / 2.0);
    // Are we processor with even ID or odd ID?
    const bool evenProc{(myId % 2) == 0};

    for (std::size_t step = 1; step <= numSteps; ++step)
    {
        // I) All processors with odd IDs merge with neighbour and merge, then redistribute.
        if (evenProc)
        { // Send my buffer to neighbour and wait for response.
            if (myId != 0)
            {
                MPI_Send(myNumbers.data(), elementsPerProcessor, SORTED_MPI_TYPE, myId - 1, MY_TAG, MPI_COMM_WORLD);
                MPI_Recv(myNumbers.data(), elementsPerProcessor, SORTED_MPI_TYPE, myId - 1, MY_TAG, MPI_COMM_WORLD, &status);
            }
        }
        else
        { // Receive from neighbour, merge and redistribute.
            if (myId != numProcs - 1)
            {
                MPI_Recv(receiveBuffer.data(), elementsPerProcessor, SORTED_MPI_TYPE, myId + 1, MY_TAG, MPI_COMM_WORLD, &status);
                std::merge(myNumbers.begin(), myNumbers.end(), receiveBuffer.begin(), receiveBuffer.end(), mergeBuffer.begin());
                std::copy(mergeBuffer.begin(), mergeBuffer.begin() + elementsPerProcessor, myNumbers.begin());
                MPI_Send(mergeBuffer.data() + elementsPerProcessor, elementsPerProcessor, SORTED_MPI_TYPE, myId + 1, MY_TAG, MPI_COMM_WORLD);
            }
        }

        // II) All processors with even IDs merge with neighbour and merge, then redistribute.
        if (evenProc)
        { // Send my buffer to neighbour and wait for response.
            if (myId != numProcs - 1)
            {
                MPI_Recv(receiveBuffer.data(), elementsPerProcessor, SORTED_MPI_TYPE, myId + 1, MY_TAG, MPI_COMM_WORLD, &status);
                std::merge(myNumbers.begin(), myNumbers.end(), receiveBuffer.begin(), receiveBuffer.end(), mergeBuffer.begin());
                std::copy(mergeBuffer.begin(), mergeBuffer.begin() + elementsPerProcessor, myNumbers.begin());
                MPI_Send(mergeBuffer.data() + elementsPerProcessor, elementsPerProcessor, SORTED_MPI_TYPE, myId + 1, MY_TAG, MPI_COMM_WORLD);
            }
        }
        else
        { // Receive from neighbour, merge and redistribute.
            if (myId != 0)
            {
                MPI_Send(myNumbers.data(), elementsPerProcessor, SORTED_MPI_TYPE, myId - 1, MY_TAG, MPI_COMM_WORLD);
                MPI_Recv(myNumbers.data(), elementsPerProcessor, SORTED_MPI_TYPE, myId - 1, MY_TAG, MPI_COMM_WORLD, &status);
            }
        }
    }
}

void removePadding(std::vector<SortedType> &sortedNumbers, std::size_t numPadded)
{
    for (std::size_t iii = 0; iii < numPadded; ++iii)
    {
        sortedNumbers.pop_back();
    }
}

/// Main application function.
int mainProgram(std::size_t numProcs, std::size_t myId, const char *filename)
{
    /// How many elements did we add to pad the input vector?
    std::size_t numPadded{0u};
    /// How many elements does one processor handle?
    std::size_t elementsPerProcessor{0u};

    /// Status used for getting information from MPI functions.
    MPI_Status status;
    /// used for getting status from MPI functions.
    int mpiRet;

    // Vector used for storing numbers the processor is working on.
    std::vector<SortedType> myNumbers;
#ifdef DEBUG
    std::vector<SortedType> myDebugInput;
#endif

    if (myId == 0)
    { // Master processor handles input.
        std::vector<SortedType> inputNumbers;
        try {
            inputNumbers = loadFile(filename);
            if (inputNumbers.size() < numProcs)
            {
                throw std::runtime_error(std::string("Too many processors to sort ")
                                         + std::to_string(inputNumbers.size()) + "elements!");
            }
        } catch (const std::runtime_error &err) {
            std::cerr << "Master processor ran into trouble (" << err.what()
                      << "), sending error size and quitting!" << std::endl;

            int size{ERROR_SIZE};
            MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);

            return EXIT_FAILURE;
        }

#ifdef DEBUG
        myDebugInput = inputNumbers;
#endif
#ifndef DEBUG
        printInputNumbers(inputNumbers);
#endif

        numPadded = padInputNumbers(inputNumbers, numProcs);

        // Broadcast size, so each processor can allocate memory.
        elementsPerProcessor = inputNumbers.size() / numProcs;
        MPI_Bcast(&elementsPerProcessor, 1, MPI_INT, 0, MPI_COMM_WORLD);

        myNumbers.resize(elementsPerProcessor);

        // Copy numbers for master processor.
        std::copy(inputNumbers.begin(), inputNumbers.begin() + elementsPerProcessor, myNumbers.begin());
        // Send rest to the other processors.
        splitInputNumbers(inputNumbers, elementsPerProcessor, numProcs);
    }
    else
    { // Others wait for size specification from the master processor.
        int size{ERROR_SIZE};
        MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);
        if (size < 0)
        { // Received the error size, we should just quit.
            return EXIT_FAILURE;
        }

        elementsPerProcessor = size;
        myNumbers.resize(static_cast<std::size_t>(elementsPerProcessor));

        mpiRet = MPI_Recv(myNumbers.data(), elementsPerProcessor, SORTED_MPI_TYPE, 0, MY_TAG, MPI_COMM_WORLD, &status);
        if (mpiRet != MPI_SUCCESS)
        {
            std::cerr << "Unable to receive data from master processor!" << std::endl;
            return EXIT_FAILURE;
        }
    }

    mergeSplittingSort(myNumbers, myId, numProcs);

    std::vector<SortedType> sortedNumbers(elementsPerProcessor * numProcs);
    MPI_Gather(myNumbers.data(), elementsPerProcessor, SORTED_MPI_TYPE, sortedNumbers.data(), elementsPerProcessor, SORTED_MPI_TYPE, 0, MPI_COMM_WORLD);

    if (myId == 0)
    { // Remove the padding.
        removePadding(sortedNumbers, numPadded);
#ifndef DEBUG
        printOutputNumbers(sortedNumbers);
#endif
#ifdef DEBUG
        std::sort(myDebugInput.begin(), myDebugInput.end());
        if (sortedNumbers != myDebugInput)
        {
            std::cerr << "Sorted input does NOT equal the test vector!" << std::endl;
        }
        else
        {
            std::cerr << "Sorted input DOES equal the test vector!" << std::endl;
        }
#endif
    }

    return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int numProcs{-1};
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    int myId{-1};
    MPI_Comm_rank(MPI_COMM_WORLD, &myId);

    if (argc < PARAM_NUM_REQUIRED)
    {
        if (myId == 0)
        {
            std::cerr << "Usage: mpirun --prefix /usr/local/share/OpenMPI -np NUM_PROCS mss PATH_TO_NUMBERS_FILE" << std::endl;
        }
        return EXIT_FAILURE;
    }

    if (numProcs < 0 || myId < 0)
    {
        std::cerr << "Got negative number of processors or my ID!" << std::endl;

        int size{ERROR_SIZE};
        MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);

        return EXIT_FAILURE;
    }

    MPI_Barrier(MPI_COMM_WORLD);
    double start = MPI_Wtime();

    int retVal{EXIT_FAILURE};
    try {
        retVal = mainProgram(static_cast<std::size_t>(numProcs),
                             static_cast<std::size_t>(myId),
                             argv[PARAM_FILENAME]);
    } catch (...) {
        MPI_Abort(MPI_COMM_WORLD, ERROR_SIZE);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    double end = MPI_Wtime();

    MPI_Finalize();

#ifdef DEBUG
    if (myId == 0)
    {
        std::cout << "Operation took " << end - start << " seconds" << std::endl;
    }
#endif

    return retVal;
#if 0
    //PRIJETI HODNOTY CISLA
    //vsechny procesory(vcetne mastera) prijmou hodnotu a zahlasi ji
    MPI_Recv(&mynumber, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat); //buffer,velikost,typ,rank odesilatele,tag, skupina, stat
    //cout<<"i am:"<<myid<<" my number is:"<<mynumber<<endl;
 
    //LIMIT PRO INDEXY
    int oddlimit= 2*(numprocs/2)-1;                 //limity pro sude
    int evenlimit= 2*((numprocs-1)/2);              //liche
    int halfcycles= numprocs/2;
    int cycles=0;                                   //pocet cyklu pro pocitani slozitosti
    //if(myid == 0) cout<<oddlimit<<":"<<evenlimit<<endl;


    //RAZENI------------chtelo by to umet pocitat cykly nebo neco na testy------
    //cyklus pro linearitu
    for(int j=1; j<=halfcycles; j++){
        cycles++;           //pocitame cykly, abysme mohli udelat krasnej graf:)

        //sude proc 
        if((!(myid%2) || myid==0) && (myid<oddlimit)){
            MPI_Send(&mynumber, 1, MPI_INT, myid+1, TAG, MPI_COMM_WORLD);          //poslu sousedovi svoje cislo
            MPI_Recv(&mynumber, 1, MPI_INT, myid+1, TAG, MPI_COMM_WORLD, &stat);   //a cekam na nizsi
            //cout<<"ss: "<<myid<<endl;
        }//if sude
        else if(myid<=oddlimit){//liche prijimaji zpravu a vraceji mensi hodnotu (to je ten swap)
            MPI_Recv(&neighnumber, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD, &stat); //jsem sudy a prijimam

            if(neighnumber > mynumber){                                             //pokud je leveho sous cislo vetsi
                MPI_Send(&mynumber, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD);       //poslu svoje 
                mynumber= neighnumber;                                              //a vemu si jeho
            }
            else MPI_Send(&neighnumber, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD);   //pokud je mensi nebo stejne vratim
            //cout<<"sl: "<<myid<<endl;
        }//else if (liche)
        else{//sem muze vlezt jen proc, co je na konci
        }//else

        //liche proc 
        if((myid%2) && (myid<evenlimit)){
            MPI_Send(&mynumber, 1, MPI_INT, myid+1, TAG, MPI_COMM_WORLD);           //poslu sousedovi svoje cislo
            MPI_Recv(&mynumber, 1, MPI_INT, myid+1, TAG, MPI_COMM_WORLD, &stat);    //a cekam na nizsi
            //cout<<"ll: "<<myid<<endl;
        }//if liche
        else if(myid<=evenlimit && myid!=0){//sude prijimaji zpravu a vraceji mensi hodnotu (to je ten swap)
            MPI_Recv(&neighnumber, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD, &stat); //jsem sudy a prijimam

            if(neighnumber > mynumber){                                             //pokud je leveho sous cislo vetsi
                MPI_Send(&mynumber, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD);       //poslu svoje 
                mynumber= neighnumber;                                              //a vemu si jeho
            }
            else MPI_Send(&neighnumber, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD);   //pokud je mensi nebo stejne vratim
            //cout<<"ls: "<<myid<<endl;
        }//else if (sude)
        else{//sem muze vlezt jen proc, co je na konci
        }//else
        
    }//for pro linearitu
    //RAZENI--------------------------------------------------------------------


    //FINALNI DISTRIBUCE VYSLEDKU K MASTEROVI-----------------------------------
    int* final= new int [numprocs];
    //final=(int*) malloc(numprocs*sizeof(int));
    for(int i=1; i<numprocs; i++){
       if(myid == i) MPI_Send(&mynumber, 1, MPI_INT, 0, TAG,  MPI_COMM_WORLD);
       if(myid == 0){
           MPI_Recv(&neighnumber, 1, MPI_INT, i, TAG, MPI_COMM_WORLD, &stat); //jsem 0 a prijimam
           final[i]=neighnumber;
       }//if sem master
    }//for

    if(myid == 0){
        //cout<<cycles<<endl;
        final[0]= mynumber;
        for(int i=0; i<numprocs; i++){
            cout<<"proc: "<<i<<" num: "<<final[i]<<endl;
        }//for
    }//if vypis
    //cout<<"i am:"<<myid<<" my number is:"<<mynumber<<endl;
    //VYSLEDKY------------------------------------------------------------------

    return EXIT_FAILURE;
#endif
}
